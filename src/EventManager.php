<?php

namespace Drupal\ga_server_events;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;

/**
 * EventManager service is used for managing the events.
 */
class EventManager {

  /**
   * State interface.
   *
   * @var \Drupal\Core\State\StateInterface
   *   Defines the state interface.
   */
  protected $state;

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   *   Defines the current user account object.
   */
  protected $currentUser;

  /**
   * Constructor for dependency injection.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The State Service for setting, getting and deleting state values.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The Current user.
   */
  public function __construct(StateInterface $state, AccountInterface $current_user) {
    $this->state = $state;
    $this->currentUser = $current_user;
  }

  /**
   * Adds an event.
   *
   * Will always be for the current user.
   */
  public function addEvent(Event $event) {
    $this->addUserEvent($this->currentUser, $event);
  }

  /**
   * Adds user.
   */
  public function addUserEvent(AccountInterface $user, $event) {
    $key = $this->createUserKey($user);
    $events = $this->state->get($key, []);
    $events[] = $event;
    $this->state->set($key, $events);
  }

  /**
   * Creates user.
   */
  protected function createUserKey(AccountInterface $user) {
    return 'ga_server_events_' . $user->id();
  }

  /**
   * Retrieves all events.
   *
   * @return \Drupal\ga_server_events\Event[]
   *   An array of events for the current user.
   */
  public function getEvents() {
    return $this->getUserEvents($this->currentUser);
  }

  /**
   * Get the user events.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user you want to check.
   *
   * @return \Drupal\ga_server_events\Event[]
   *   An array of events for the specified user.
   */
  public function getUserEvents(AccountInterface $user) {
    return $this->state->get('ga_server_events_' . $user->id(), []);
  }

  /**
   * Clears the events for the current user.
   */
  public function clearEvents() {
    $this->clearUserEvents($this->currentUser);
  }

  /**
   * Clears the events for the specified user.
   */
  public function clearUserEvents(AccountInterface $user) {
    $key = $this->createUserKey($user);
    $this->state->set($key, []);
    $this->state->resetCache();
  }

}
