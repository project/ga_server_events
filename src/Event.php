<?php

namespace Drupal\ga_server_events;

/**
 * Defines the Event class.
 */
class Event {

  /**
   * The hit type.
   *
   * @var string
   *   Describes the variable string.
   */
  protected $hitType = '';

  /**
   * The an event category of type string.
   *
   * @var string
   */
  protected $eventCategory = '';

  /**
   * Describes the event action.
   *
   * @var string
   */
  protected $eventAction = '';

  /**
   * Describes the event label.
   *
   * @var string
   */
  protected $eventLabel = '';

  /**
   * Returns the hit type.
   *
   * @return string
   *   Getter method for the hit type.
   */
  public function getHitType() {
    return $this->hitType;
  }

  /**
   * Sets the hit type.
   *
   * @param string $hitType
   *   Setter method for the hit type.
   */
  public function setHitType($hitType) {
    $this->hitType = $hitType;
  }

  /**
   * Gets the event category.
   *
   * @return string
   *   Getter method for the event category.
   */
  public function getEventCategory() {
    return $this->eventCategory;
  }

  /**
   * Sets event category.
   *
   * @param string $eventCategory
   *   Setter method for the event category.
   */
  public function setEventCategory($eventCategory) {
    $this->eventCategory = $eventCategory;
  }

  /**
   * Gets the event action.
   *
   * @return string
   *   Getter method for the event action.
   */
  public function getEventAction() {
    return $this->eventAction;
  }

  /**
   * Sets the event action.
   *
   * @param string $eventAction
   *   Setter method for the event action.
   */
  public function setEventAction($eventAction) {
    $this->eventAction = $eventAction;
  }

  /**
   * Gets the event label.
   *
   * @return string
   *   Getter method for the event label.
   */
  public function getEventLabel() {
    return $this->eventLabel;
  }

  /**
   * Sets the event label.
   *
   * @param string $eventLabel
   *   Setter method for the event label.
   */
  public function setEventLabel($eventLabel) {
    $this->eventLabel = $eventLabel;
  }

  /**
   * Creates from values.
   */
  public static function createFromValues(array $values) {
    $e = new self();
    foreach ($values as $key => $value) {
      if (!property_exists(self::class, $key)) {
        throw new \InvalidArgumentException('No such GA property: ' . $key);
      }
      $e->{$key} = $value;
    }
    return $e;
  }

  /**
   * Gets the script.
   */
  public function getScript() {
    return sprintf("ga('send', {
      hitType: '%s',
      eventCategory: '%s',
      eventAction: '%s',
      eventLabel: '%s'
    });", $this->getHitType(), $this->getEventCategory(), $this->getEventAction(), $this->getEventLabel());
  }

}
