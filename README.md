# Google analytics server triggered events

Ever wanted to trigger a google analytics event when something happens server
side? Like a node was saved, a user created or maybe a purchase completed?
This module enables you to do that.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ga_server_events).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ga_server_events).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Google Analytics](https://www.drupal.org/project/google_analytics)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Allow the google analytics server permissions.


## Maintainers

- Eirik Morland - [eiriksm](https://www.drupal.org/u/eiriksm)
